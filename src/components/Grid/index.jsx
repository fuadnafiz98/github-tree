import { useEffect, useState } from "react";
import Split from "react-split";
import "./index.css";
import Left from "./Left";
import Right from "./Right";
import getTree from "../../data/getTree";

const USER = "fuadnafiz98";
const REPO = "github-action";
// const TOKEN = "64564808a8807b86246d81669117badbf2a69d5a";

const Grid = () => {
  const [code, setCode] = useState("");
  const [root, setRoot] = useState({});
  const [type, setType] = useState("");
  const [content, setContent] = useState("");

  useEffect(() => {
    async function fetchTree() {
      const row = await fetch(
        `https://api.github.com/repos/${USER}/${REPO}/git/trees/master?recursive=true`,
        {
          headers: {
            Authorization: "token 64564808a8807b86246d81669117badbf2a69d5a",
          },
        }
      );
      const json = await row.json();
      const root = getTree(json);
      setRoot(root);
    }
    fetchTree();
  }, []);

  useEffect(() => {
    console.log("fetch data", code);
    const fetchData = async () => {
      const response = await fetch(
        `https://raw.githubusercontent.com/${USER}/${REPO}/master${code}`,
        {
          headers: {
            Authorization: "token 64564808a8807b86246d81669117badbf2a69d5a",
          },
        }
      );
      const raw = await response.blob();
      if (raw.type === "text/plain") {
        setType("text");
        const data = await new Response(raw).text();
        console.log(data);
        setContent(data);
      } else if (raw.type.match("image")) {
        const url = URL.createObjectURL(raw);
        setType("img");
        setContent(url);
      } else {
        const url = URL.createObjectURL(raw);
        setType("url");
        setContent(url);
      }
    };
    return fetchData();
  }, [code, setCode]);

  return (
    <div className="bg-nw-bg">
      <Split
        gutterSize={5}
        sizes={[25, 75]}
        minSize={[200, 400]}
        expandToMin={true}
        cursor="col-resize"
        className="flex h-full"
      >
        {/* <div className="flex h-full m-4"> */}
        <div className="max-h-screen text-nw-fg">
          <Left handleRight={setCode} child={root} path="" move={1} />
        </div>
        <div className="h-screen overflow-ellipsis">
          <Right code={code} type={type} content={content} />
        </div>
        {/* </div> */}
      </Split>
    </div>
  );
};

export default Grid;
