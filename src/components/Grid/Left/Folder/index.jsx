import File from "../File";
import { useState } from "react";
import FolderOpen from "../../../../static/svg/FolderOpen";

const Folder = ({ title, child, move, handleRight, path }) => {
  const [toggle, setToggle] = useState(false);
  return (
    <div className={`ml-${move + 1} bg-nw-bg`}>
      <div
        className="flex items-center pb-1 space-x-2 border-b-2 border-gray-500 border-opacity-25 cursor-pointer"
        onClick={() => {
          setToggle(!toggle);
        }}
      >
        <span>
          <FolderOpen className={`${toggle ? "fill-current" : ""} w-4 h-4`} />
        </span>
        {/* <h2>{title !== "files" ? title : ""}</h2> */}
        <h2>{title}</h2>
      </div>
      {toggle &&
        Object.keys(child).map((fold, i) => {
          // console.log(typeof child[fold]);
          if (child[fold] === "string") {
            return (
              <div
                className={`flex items-center pb-1 space-x-2 border-b-2 border-gray-600 border-opacity-25 cursor-pointer
              }`}
              >
                {/* <span>
                  <FileOpen className="w-3 h-3" />
                </span>
                <h2>{child[fold]}</h2> */}
                <File
                  path={`${path}/${child[fold]}`}
                  title={child[fold]}
                  handleRight={handleRight}
                />
              </div>
            );
          }
          // for initial root files, the array is called "files"
          else if (fold === "files") {
            return child[fold].map((files, i) => (
              <div
                key={i}
                className={`flex items-center pb-1 space-x-2 border-b-2 border-gray-600 border-opacity-25 cursor-pointer pl-1 ml-${
                  move + 1
                }`}
              >
                <File
                  path={`${path}/${files}`}
                  title={files}
                  handleRight={handleRight}
                />
              </div>
            ));
          } else {
            return (
              <div key={i}>
                <Folder
                  title={fold}
                  child={child[fold]}
                  move={move + 1}
                  handleRight={handleRight}
                  path={`${path}/${fold}`}
                />
              </div>
            );
          }
        })}
    </div>
  );
};
export default Folder;

/*
 return (
    <div className={`bg-purple-400 ml-${move + 1}`}>
      <h2>{title !== "files" ? title : ""}</h2>
      {Object.keys(child).map((fold, i) =>
        // (fold, i) => console.log(typeof child[fold])
        typeof child[fold] == "string" ? (
          <div key={i}>
            <File title={child[fold]} move={move + 1} />
          </div>
        ) : (
          <div key={i}>
            <Folder title={fold} child={child[fold]} move={move + 1} />
          </div>
        )
      )}

    </div>
  );
*/
