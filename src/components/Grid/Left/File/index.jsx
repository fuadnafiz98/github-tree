import FileOpen from "../../../../static/svg/FileOpen";

const Single = ({ title, path, handleRight }) => {
  return (
    <div className="flex space-x-2">
      <span>
        <FileOpen className="w-3 h-3" />
      </span>
      <button
        onClick={e => {
          e.preventDefault();
          handleRight(path);
        }}
      >
        {title}
      </button>
    </div>
  );
};

export default Single;
