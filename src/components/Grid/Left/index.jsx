import File from "./File";
import Folder from "./Folder";

const Left = ({ handleRight, child, move, path }) => {
  return (
    <div className="h-screen overflow-scroll">
      {Object.keys(child).map((fold, i) =>
        typeof child[fold] == "object" && fold !== "files" ? (
          // console.log(key, child[key]);
          <div key={i} className={`ml-${move + 1}`}>
            <Folder
              path={`${path}/${fold}`}
              handleRight={handleRight}
              title={fold}
              child={child[fold]}
              move={move + 1}
            />
          </div>
        ) : (
          child[fold].map((files, i) => (
            <div
              key={i}
              className={`flex items-center pb-1 space-x-2 border-b-2 border-gray-600 border-opacity-25 cursor-pointer ml-${
                move + 1
              }`}
            >
              <File
                path={`${path}/${files}`}
                title={files}
                handleRight={handleRight}
              />
            </div>
          ))
        )
      )}
    </div>
  );
};

export default Left;
