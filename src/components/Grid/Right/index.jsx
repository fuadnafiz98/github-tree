import Prism from "prismjs";
import "prism-theme-night-owl";

import { useEffect } from "react";

const name = str => str.split("/").pop();
const extension = str => str.split(".").pop();

const Right = ({ code, type, content }) => {
  // useEffect(() => {
  //   Prism.highlightAll();
  // }, [content, type, code]);
  useEffect(() => {
    Prism.highlightAll();
  }, [content]);
  return (
    <div>
      {type === "img" && <img src={content} alt="random" />}
      {type === "text" && (
        <div>
          <h2>{extension(code)}</h2>
          <pre>
            <code className={`language-${extension(code)}`}>{content}</code>
          </pre>
        </div>
      )}
      {type === "url" && (
        <div>
          <a href={content} download={name(code)}>
            {name(code)}
          </a>
        </div>
      )}
    </div>
  );
};

export default Right;
