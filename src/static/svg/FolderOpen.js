function FolderOpen(props) {
  return (
    <svg
      width={20}
      height={16}
      viewBox="0 0 20 16"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M1 3v10a2 2 0 002 2h14a2 2 0 002-2V5a2 2 0 00-2-2h-6L9 1H3a2 2 0 00-2 2z"
        stroke="#D6DEEB"
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
}

export default FolderOpen;
