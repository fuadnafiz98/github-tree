import { useEffect } from "react";
import Grid from "./components/Grid";

function App() {
  useEffect(() => {
    console.log("hello world");
  }, []);
  return (
    <div className="h-auto font-mono text-xs text-gray-100">
      <Grid />
    </div>
  );
}

export default App;
