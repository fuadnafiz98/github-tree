// import { data } from "./data";
const deepMerge = require("deepmerge");

// const files = data.tree;

// const n = files.length;

const getTree = data => {
  const files = data.tree;

  const n = files.length;
  let root = {};

  for (let i = 0; i < n; i++) {
    const path = files[i].path;
    const type = files[i].type;
    const fileNames = path.split("/");
    let ob = {};
    if (type === "tree") {
      ob = {};
    } else {
      ob = {
        files: [fileNames[fileNames.length - 1]],
      };
    }
    let obj = {};
    for (let j = fileNames.length - 1; j > 0; j--) {
      obj[fileNames[j - 1]] = ob;
      ob = obj;
      obj = {};
    }
    root = deepMerge(root, ob);
  }
  return root;
};
export default getTree;
