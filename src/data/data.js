export const data = {
  sha: "f318c751dd75c3f92019734fc53564ca6b2809df",
  url:
    "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/trees/f318c751dd75c3f92019734fc53564ca6b2809df",
  tree: [
    {
      path: ".editorconfig",
      mode: "100644",
      type: "blob",
      sha: "6537ca4677ee97ddcb112f22d086b92721fd578c",
      size: 220,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/6537ca4677ee97ddcb112f22d086b92721fd578c",
    },
    {
      path: ".env.example",
      mode: "100644",
      type: "blob",
      sha: "ac748637ae52e837bedc4234dbcad2f82e5187e7",
      size: 778,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/ac748637ae52e837bedc4234dbcad2f82e5187e7",
    },
    {
      path: ".eslintrc.json",
      mode: "100644",
      type: "blob",
      sha: "3c2a552dc888ebaed5d8294a3fc279fdc5a32088",
      size: 646,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/3c2a552dc888ebaed5d8294a3fc279fdc5a32088",
    },
    {
      path: ".gitattributes",
      mode: "100644",
      type: "blob",
      sha: "967315dd3d16d50942fa7abd383dfb95ec685491",
      size: 111,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/967315dd3d16d50942fa7abd383dfb95ec685491",
    },
    {
      path: ".gitignore",
      mode: "100644",
      type: "blob",
      sha: "b5384aa8e94e0b0346807cb5010a978bec084705",
      size: 234,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/b5384aa8e94e0b0346807cb5010a978bec084705",
    },
    {
      path: ".prettierrc",
      mode: "100644",
      type: "blob",
      sha: "87bd656d047d6f425a282f50e3d8ce1c125bca3e",
      size: 47,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/87bd656d047d6f425a282f50e3d8ce1c125bca3e",
    },
    {
      path: ".styleci.yml",
      mode: "100644",
      type: "blob",
      sha: "1db61d96e7561f2e9848c246bb0816dc20c26225",
      size: 174,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/1db61d96e7561f2e9848c246bb0816dc20c26225",
    },
    {
      path: "README.md",
      mode: "100644",
      type: "blob",
      sha: "f3decb12334f80c2f5825e350f66d1310937b874",
      size: 3738,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/f3decb12334f80c2f5825e350f66d1310937b874",
    },
    {
      path: "app",
      mode: "040000",
      type: "tree",
      sha: "dd6ef3c82aa0da18e2f9095f41018bb4e379ec4f",
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/trees/dd6ef3c82aa0da18e2f9095f41018bb4e379ec4f",
    },
    {
      path: "app/Console",
      mode: "040000",
      type: "tree",
      sha: "e5ed07d86b34f0cd0a613de0bd0e1fed5c7d051f",
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/trees/e5ed07d86b34f0cd0a613de0bd0e1fed5c7d051f",
    },
    {
      path: "app/Console/Kernel.php",
      mode: "100644",
      type: "blob",
      sha: "69914e9937839b2a0fa23ec9142ca20fd7ab7a8d",
      size: 827,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/69914e9937839b2a0fa23ec9142ca20fd7ab7a8d",
    },
    {
      path: "app/Exceptions",
      mode: "040000",
      type: "tree",
      sha: "69f34785ce33d0a895687060d38c9973fc89c45b",
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/trees/69f34785ce33d0a895687060d38c9973fc89c45b",
    },
    {
      path: "app/Exceptions/Handler.php",
      mode: "100644",
      type: "blob",
      sha: "5a53cd3babd19d20016061e4d5933b1c33e73249",
      size: 1141,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/5a53cd3babd19d20016061e4d5933b1c33e73249",
    },
    {
      path: "app/Http",
      mode: "040000",
      type: "tree",
      sha: "9bdee9a0a382a4ca30793722df3163c2bbe456b2",
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/trees/9bdee9a0a382a4ca30793722df3163c2bbe456b2",
    },
    {
      path: "app/Http/Controllers",
      mode: "040000",
      type: "tree",
      sha: "7b6988fdcafbb554b41da9623343edeaf85a9b72",
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/trees/7b6988fdcafbb554b41da9623343edeaf85a9b72",
    },
    {
      path: "app/Http/Controllers/Auth",
      mode: "040000",
      type: "tree",
      sha: "e64a138f3c427ec39c360de4e09003d74ae4e2b0",
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/trees/e64a138f3c427ec39c360de4e09003d74ae4e2b0",
    },
    {
      path: "app/Http/Controllers/Auth/ConfirmPasswordController.php",
      mode: "100644",
      type: "blob",
      sha: "138c1f08a2b2d32764fffe6ae261180dbcc5deec",
      size: 1024,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/138c1f08a2b2d32764fffe6ae261180dbcc5deec",
    },
    {
      path: "app/Http/Controllers/Auth/ForgotPasswordController.php",
      mode: "100644",
      type: "blob",
      sha: "465c39ccf90e6ef4c4c673da45209828155f7908",
      size: 667,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/465c39ccf90e6ef4c4c673da45209828155f7908",
    },
    {
      path: "app/Http/Controllers/Auth/LoginController.php",
      mode: "100644",
      type: "blob",
      sha: "18a0d088ae8c17960926578f539876764cfea417",
      size: 1002,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/18a0d088ae8c17960926578f539876764cfea417",
    },
    {
      path: "app/Http/Controllers/Auth/RegisterController.php",
      mode: "100644",
      type: "blob",
      sha: "c6a6de672709676254ebf05f4ddd5597972645cd",
      size: 1950,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/c6a6de672709676254ebf05f4ddd5597972645cd",
    },
    {
      path: "app/Http/Controllers/Auth/ResetPasswordController.php",
      mode: "100644",
      type: "blob",
      sha: "b1726a36483c8f0fce4d80b65bab46a873f10dab",
      size: 844,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/b1726a36483c8f0fce4d80b65bab46a873f10dab",
    },
    {
      path: "app/Http/Controllers/Auth/VerificationController.php",
      mode: "100644",
      type: "blob",
      sha: "5e749af86f4351bbba50052c34084f66a30c2f65",
      size: 1130,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/5e749af86f4351bbba50052c34084f66a30c2f65",
    },
    {
      path: "app/Http/Controllers/Controller.php",
      mode: "100644",
      type: "blob",
      sha: "a0a2a8a34a6221e4dceb24a759ed14e911f74c57",
      size: 361,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/a0a2a8a34a6221e4dceb24a759ed14e911f74c57",
    },
    {
      path: "app/Http/Controllers/HomeController.php",
      mode: "100644",
      type: "blob",
      sha: "7cbc2c3f0cbc4fd1d2b2734a0301b416f54f479e",
      size: 467,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/7cbc2c3f0cbc4fd1d2b2734a0301b416f54f479e",
    },
    {
      path: "app/Http/Kernel.php",
      mode: "100644",
      type: "blob",
      sha: "36ced134a157580774c32a84a3fcb91c686a2362",
      size: 2466,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/36ced134a157580774c32a84a3fcb91c686a2362",
    },
    {
      path: "app/Http/Middleware",
      mode: "040000",
      type: "tree",
      sha: "6e67e55e9cf0ce9ddb2502a41a9a7cb62c1e85a6",
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/trees/6e67e55e9cf0ce9ddb2502a41a9a7cb62c1e85a6",
    },
    {
      path: "app/Http/Middleware/Authenticate.php",
      mode: "100644",
      type: "blob",
      sha: "704089a7fe757c137d99241b758c912d8391e19d",
      size: 469,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/704089a7fe757c137d99241b758c912d8391e19d",
    },
    {
      path: "app/Http/Middleware/CheckForMaintenanceMode.php",
      mode: "100644",
      type: "blob",
      sha: "35b9824baefb8118ada8e6d03f62278f00d1028e",
      size: 335,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/35b9824baefb8118ada8e6d03f62278f00d1028e",
    },
    {
      path: "app/Http/Middleware/EncryptCookies.php",
      mode: "100644",
      type: "blob",
      sha: "033136ad128b7687558bc41b03cc92f39cf69149",
      size: 294,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/033136ad128b7687558bc41b03cc92f39cf69149",
    },
    {
      path: "app/Http/Middleware/RedirectIfAuthenticated.php",
      mode: "100644",
      type: "blob",
      sha: "2395ddccf9e278a719bac028ce90d5be0c2ebd8e",
      size: 582,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/2395ddccf9e278a719bac028ce90d5be0c2ebd8e",
    },
    {
      path: "app/Http/Middleware/TrimStrings.php",
      mode: "100644",
      type: "blob",
      sha: "5a50e7b5c8bd8a5c853ee7edb8170508f2ff4f86",
      size: 340,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/5a50e7b5c8bd8a5c853ee7edb8170508f2ff4f86",
    },
    {
      path: "app/Http/Middleware/TrustHosts.php",
      mode: "100644",
      type: "blob",
      sha: "b0550cfc7c61f0220b5cf7ee6a1ef2d8c4a4cab7",
      size: 354,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/b0550cfc7c61f0220b5cf7ee6a1ef2d8c4a4cab7",
    },
    {
      path: "app/Http/Middleware/TrustProxies.php",
      mode: "100644",
      type: "blob",
      sha: "14befceb006114d1b6bb607bd0cfcf489dd50f42",
      size: 441,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/14befceb006114d1b6bb607bd0cfcf489dd50f42",
    },
    {
      path: "app/Http/Middleware/VerifyCsrfToken.php",
      mode: "100644",
      type: "blob",
      sha: "0c13b85489692e3a6d7cd37c3f3e428291ff3ad4",
      size: 307,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/0c13b85489692e3a6d7cd37c3f3e428291ff3ad4",
    },
    {
      path: "app/Providers",
      mode: "040000",
      type: "tree",
      sha: "3764393b597cc0dde98458406b7ab84cd56f8dd2",
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/trees/3764393b597cc0dde98458406b7ab84cd56f8dd2",
    },
    {
      path: "app/Providers/AppServiceProvider.php",
      mode: "100644",
      type: "blob",
      sha: "ee8ca5bcd8f77d219f29529a9163587235c545d5",
      size: 403,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/ee8ca5bcd8f77d219f29529a9163587235c545d5",
    },
    {
      path: "app/Providers/AuthServiceProvider.php",
      mode: "100644",
      type: "blob",
      sha: "30490683b9eb0b302ec899af306ff9b29102fd17",
      size: 578,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/30490683b9eb0b302ec899af306ff9b29102fd17",
    },
    {
      path: "app/Providers/BroadcastServiceProvider.php",
      mode: "100644",
      type: "blob",
      sha: "395c518bc47b94752d00b8dc7aeb7d241633e7cf",
      size: 380,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/395c518bc47b94752d00b8dc7aeb7d241633e7cf",
    },
    {
      path: "app/Providers/EventServiceProvider.php",
      mode: "100644",
      type: "blob",
      sha: "723a290d57d86b15a0da5924b60220a161b2967f",
      size: 710,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/723a290d57d86b15a0da5924b60220a161b2967f",
    },
    {
      path: "app/Providers/RouteServiceProvider.php",
      mode: "100644",
      type: "blob",
      sha: "540d17b43083574b9ad5185c4982b55f18e31607",
      size: 1658,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/540d17b43083574b9ad5185c4982b55f18e31607",
    },
    {
      path: "app/User.php",
      mode: "100644",
      type: "blob",
      sha: "e79dab7fea8f6601eaf07072ee6b12fbe20bdbe1",
      size: 734,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/e79dab7fea8f6601eaf07072ee6b12fbe20bdbe1",
    },
    {
      path: "artisan",
      mode: "100644",
      type: "blob",
      sha: "5c23e2e24fc5d9e8224d7357dbb583c83884582b",
      size: 1686,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/5c23e2e24fc5d9e8224d7357dbb583c83884582b",
    },
    {
      path: "bootstrap",
      mode: "040000",
      type: "tree",
      sha: "fa579600b150dfe96277f923c509bc473517b32a",
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/trees/fa579600b150dfe96277f923c509bc473517b32a",
    },
    {
      path: "bootstrap/app.php",
      mode: "100644",
      type: "blob",
      sha: "037e17df03b0598d7bbd27ed333312e8e337fb1b",
      size: 1620,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/037e17df03b0598d7bbd27ed333312e8e337fb1b",
    },
    {
      path: "bootstrap/cache",
      mode: "040000",
      type: "tree",
      sha: "96233b34ccba706a9f89dca87a9282a3cd836e0a",
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/trees/96233b34ccba706a9f89dca87a9282a3cd836e0a",
    },
    {
      path: "bootstrap/cache/.gitignore",
      mode: "100644",
      type: "blob",
      sha: "d6b7ef32c8478a48c3994dcadc86837f4371184d",
      size: 14,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/d6b7ef32c8478a48c3994dcadc86837f4371184d",
    },
    {
      path: "composer.json",
      mode: "100644",
      type: "blob",
      sha: "6725f2564aa5d1dfec4e149293394bca7bbfa169",
      size: 1638,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/6725f2564aa5d1dfec4e149293394bca7bbfa169",
    },
    {
      path: "composer.lock",
      mode: "100644",
      type: "blob",
      sha: "33dc00ab367a360c9d3d2579bb7e61b1d681f6d5",
      size: 245073,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/33dc00ab367a360c9d3d2579bb7e61b1d681f6d5",
    },
    {
      path: "config",
      mode: "040000",
      type: "tree",
      sha: "6c6e1eb225d1f174fc83d33884d3b99b6827f124",
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/trees/6c6e1eb225d1f174fc83d33884d3b99b6827f124",
    },
    {
      path: "config/app.php",
      mode: "100644",
      type: "blob",
      sha: "8409e00ea9fc20548d3ce10d6af12607bad3e0af",
      size: 9239,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/8409e00ea9fc20548d3ce10d6af12607bad3e0af",
    },
    {
      path: "config/auth.php",
      mode: "100644",
      type: "blob",
      sha: "aaf982bcdcee30074e582002f9832c2e5621f639",
      size: 3796,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/aaf982bcdcee30074e582002f9832c2e5621f639",
    },
    {
      path: "config/broadcasting.php",
      mode: "100644",
      type: "blob",
      sha: "3bba1103e602a5743e62dae09ee31a4765b7bf51",
      size: 1601,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/3bba1103e602a5743e62dae09ee31a4765b7bf51",
    },
    {
      path: "config/cache.php",
      mode: "100644",
      type: "blob",
      sha: "4f41fdf966b5b8a753eab42f6b4c5fcdcd944a0f",
      size: 3100,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/4f41fdf966b5b8a753eab42f6b4c5fcdcd944a0f",
    },
    {
      path: "config/cors.php",
      mode: "100644",
      type: "blob",
      sha: "558369dca41bf7de39f68917cdfc61732082cf08",
      size: 823,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/558369dca41bf7de39f68917cdfc61732082cf08",
    },
    {
      path: "config/database.php",
      mode: "100644",
      type: "blob",
      sha: "b42d9b30a5463e6f1c739724d5debdc487f94748",
      size: 5054,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/b42d9b30a5463e6f1c739724d5debdc487f94748",
    },
    {
      path: "config/filesystems.php",
      mode: "100644",
      type: "blob",
      sha: "94c81126b227e2f469670b1921b6a84e4023af4c",
      size: 2688,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/94c81126b227e2f469670b1921b6a84e4023af4c",
    },
    {
      path: "config/hashing.php",
      mode: "100644",
      type: "blob",
      sha: "842577087c0c8c1beae4882d39504a04a207b28a",
      size: 1571,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/842577087c0c8c1beae4882d39504a04a207b28a",
    },
    {
      path: "config/logging.php",
      mode: "100644",
      type: "blob",
      sha: "088c204e29942a941c9cbfb8dadb44671a5b1814",
      size: 2896,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/088c204e29942a941c9cbfb8dadb44671a5b1814",
    },
    {
      path: "config/mail.php",
      mode: "100644",
      type: "blob",
      sha: "54299aabf8ade237c98d2ab199f1c3624ae30f59",
      size: 3372,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/54299aabf8ade237c98d2ab199f1c3624ae30f59",
    },
    {
      path: "config/queue.php",
      mode: "100644",
      type: "blob",
      sha: "00b76d651812116a377978d7bda5df3a48c5f9ba",
      size: 2760,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/00b76d651812116a377978d7bda5df3a48c5f9ba",
    },
    {
      path: "config/services.php",
      mode: "100644",
      type: "blob",
      sha: "2a1d616c77464de6df0a2e6b90d5dba529dbd856",
      size: 950,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/2a1d616c77464de6df0a2e6b90d5dba529dbd856",
    },
    {
      path: "config/session.php",
      mode: "100644",
      type: "blob",
      sha: "4e0f66cda64c5f324c991d95992a5792523f043a",
      size: 7041,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/4e0f66cda64c5f324c991d95992a5792523f043a",
    },
    {
      path: "config/view.php",
      mode: "100644",
      type: "blob",
      sha: "22b8a18d325814f221fb0481fa7ab320b612d601",
      size: 1053,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/22b8a18d325814f221fb0481fa7ab320b612d601",
    },
    {
      path: "database",
      mode: "040000",
      type: "tree",
      sha: "c47143d5e237ceadbc333f87c79ef1125c3b665d",
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/trees/c47143d5e237ceadbc333f87c79ef1125c3b665d",
    },
    {
      path: "database/.gitignore",
      mode: "100644",
      type: "blob",
      sha: "97fc976772abfa2c511ea712bfdc55c97856e2fd",
      size: 26,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/97fc976772abfa2c511ea712bfdc55c97856e2fd",
    },
    {
      path: "database/factories",
      mode: "040000",
      type: "tree",
      sha: "60f5b714b30120ce333ce48e2b05f8aa7a6844a2",
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/trees/60f5b714b30120ce333ce48e2b05f8aa7a6844a2",
    },
    {
      path: "database/factories/UserFactory.php",
      mode: "100644",
      type: "blob",
      sha: "741edead6192a670fb2b10e4fbf303ab2ab3be12",
      size: 876,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/741edead6192a670fb2b10e4fbf303ab2ab3be12",
    },
    {
      path: "database/migrations",
      mode: "040000",
      type: "tree",
      sha: "b54724e771385b6e20c2e522e6fef5a8a38a5f8a",
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/trees/b54724e771385b6e20c2e522e6fef5a8a38a5f8a",
    },
    {
      path: "database/migrations/2014_10_12_000000_create_users_table.php",
      mode: "100644",
      type: "blob",
      sha: "621a24eb734b76ca474bb8b5ea4a62b5f27d223c",
      size: 798,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/621a24eb734b76ca474bb8b5ea4a62b5f27d223c",
    },
    {
      path:
        "database/migrations/2014_10_12_100000_create_password_resets_table.php",
      mode: "100644",
      type: "blob",
      sha: "0ee0a36a4f87c2f914288abfc133573d6a6324b7",
      size: 683,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/0ee0a36a4f87c2f914288abfc133573d6a6324b7",
    },
    {
      path:
        "database/migrations/2019_08_19_000000_create_failed_jobs_table.php",
      mode: "100644",
      type: "blob",
      sha: "9bddee36cb3f0b9f4942b46fc7601d457ec4390e",
      size: 774,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/9bddee36cb3f0b9f4942b46fc7601d457ec4390e",
    },
    {
      path: "database/seeds",
      mode: "040000",
      type: "tree",
      sha: "7130720c6b286316215dec3d8b53ec54d3604663",
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/trees/7130720c6b286316215dec3d8b53ec54d3604663",
    },
    {
      path: "database/seeds/DatabaseSeeder.php",
      mode: "100644",
      type: "blob",
      sha: "237dfc5d0d821135ea73d4f66d006d4337812ddc",
      size: 244,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/237dfc5d0d821135ea73d4f66d006d4337812ddc",
    },
    {
      path: "package-lock.json",
      mode: "100644",
      type: "blob",
      sha: "946df8a18513e0620736d3a319e0965795c24fcb",
      size: 537238,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/946df8a18513e0620736d3a319e0965795c24fcb",
    },
    {
      path: "package.json",
      mode: "100644",
      type: "blob",
      sha: "0247130809947c67494b6d24530e66ea717acc07",
      size: 1448,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/0247130809947c67494b6d24530e66ea717acc07",
    },
    {
      path: "phpunit.xml",
      mode: "100644",
      type: "blob",
      sha: "76f224629066a04ae8043e9bd348ad30e0532858",
      size: 1215,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/76f224629066a04ae8043e9bd348ad30e0532858",
    },
    {
      path: "postcss.config.js",
      mode: "100644",
      type: "blob",
      sha: "2ce518bbc9b90a8409650f914eb8677e720d8aa6",
      size: 83,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/2ce518bbc9b90a8409650f914eb8677e720d8aa6",
    },
    {
      path: "public",
      mode: "040000",
      type: "tree",
      sha: "f35ebed499b2424d740c5bad045f3a776825f84d",
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/trees/f35ebed499b2424d740c5bad045f3a776825f84d",
    },
    {
      path: "public/.htaccess",
      mode: "100644",
      type: "blob",
      sha: "3aec5e27e5db801fa9e321c0a97acbb49e10908f",
      size: 603,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/3aec5e27e5db801fa9e321c0a97acbb49e10908f",
    },
    {
      path: "public/css",
      mode: "040000",
      type: "tree",
      sha: "9c4413abfa7e2d6c36e606c78eea9f4bf46d6091",
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/trees/9c4413abfa7e2d6c36e606c78eea9f4bf46d6091",
    },
    {
      path: "public/css/app.css",
      mode: "100644",
      type: "blob",
      sha: "7870f78a10a9e2d67ba5e0ab646df7af9f518744",
      size: 2396974,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/7870f78a10a9e2d67ba5e0ab646df7af9f518744",
    },
    {
      path: "public/favicon.ico",
      mode: "100644",
      type: "blob",
      sha: "e69de29bb2d1d6434b8b29ae775ad8c2e48c5391",
      size: 0,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/e69de29bb2d1d6434b8b29ae775ad8c2e48c5391",
    },
    {
      path: "public/index.php",
      mode: "100644",
      type: "blob",
      sha: "4584cbcd6ad53d355c4448fc0823febd90fb5340",
      size: 1823,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/4584cbcd6ad53d355c4448fc0823febd90fb5340",
    },
    {
      path: "public/js",
      mode: "040000",
      type: "tree",
      sha: "5946e1c3a97f06b2c03ee94118e6977098ed4ce7",
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/trees/5946e1c3a97f06b2c03ee94118e6977098ed4ce7",
    },
    {
      path: "public/js/app.js",
      mode: "100644",
      type: "blob",
      sha: "160328faaecec2797620c6c8f4e0a4f8f3deff1a",
      size: 1012226,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/160328faaecec2797620c6c8f4e0a4f8f3deff1a",
    },
    {
      path: "public/mix-manifest.json",
      mode: "100644",
      type: "blob",
      sha: "4d839e606da5da43d3c13ab3455d67693fd0407c",
      size: 359,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/4d839e606da5da43d3c13ab3455d67693fd0407c",
    },
    {
      path: "public/robots.txt",
      mode: "100644",
      type: "blob",
      sha: "eb0536286f3081c6c0646817037faf5446e3547d",
      size: 24,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/eb0536286f3081c6c0646817037faf5446e3547d",
    },
    {
      path: "public/web.config",
      mode: "100644",
      type: "blob",
      sha: "d3711d7c5b8e302ccd0e3a6e32250391028e00da",
      size: 1194,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/d3711d7c5b8e302ccd0e3a6e32250391028e00da",
    },
    {
      path: "resources",
      mode: "040000",
      type: "tree",
      sha: "0f5b6f6a9b0690e128474486648fad08b6831fb0",
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/trees/0f5b6f6a9b0690e128474486648fad08b6831fb0",
    },
    {
      path: "resources/js",
      mode: "040000",
      type: "tree",
      sha: "2ab02bcab17bcad74759651d9d7e147ee9a87ca7",
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/trees/2ab02bcab17bcad74759651d9d7e147ee9a87ca7",
    },
    {
      path: "resources/js/app.js",
      mode: "100644",
      type: "blob",
      sha: "4e71b6e7786ff24ee1034e4ce237633ff6ce4f28",
      size: 219,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/4e71b6e7786ff24ee1034e4ce237633ff6ce4f28",
    },
    {
      path: "resources/js/bootstrap.js",
      mode: "100644",
      type: "blob",
      sha: "669f18819dc36f0b2e9d640eeaae86ab455a98ba",
      size: 838,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/669f18819dc36f0b2e9d640eeaae86ab455a98ba",
    },
    {
      path: "resources/js/components",
      mode: "040000",
      type: "tree",
      sha: "32adcd4cb56e74027dd8fe0f5e613dd9db634bd6",
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/trees/32adcd4cb56e74027dd8fe0f5e613dd9db634bd6",
    },
    {
      path: "resources/js/components/Example.js",
      mode: "100644",
      type: "blob",
      sha: "4d9a1911298f85144fefaeccc40dcdc048845a79",
      size: 213,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/4d9a1911298f85144fefaeccc40dcdc048845a79",
    },
    {
      path: "resources/lang",
      mode: "040000",
      type: "tree",
      sha: "dc0767233afca38db4aa38be954913a20d67bc24",
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/trees/dc0767233afca38db4aa38be954913a20d67bc24",
    },
    {
      path: "resources/lang/en",
      mode: "040000",
      type: "tree",
      sha: "1dd7b9df827b7a9c0e1345ce64f2ac2494ffc1b5",
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/trees/1dd7b9df827b7a9c0e1345ce64f2ac2494ffc1b5",
    },
    {
      path: "resources/lang/en/auth.php",
      mode: "100644",
      type: "blob",
      sha: "e5506df2907a7c8f63f3841a918611b93d67e84e",
      size: 617,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/e5506df2907a7c8f63f3841a918611b93d67e84e",
    },
    {
      path: "resources/lang/en/pagination.php",
      mode: "100644",
      type: "blob",
      sha: "d48141187786931ec2cf8645e384be7878c7dc53",
      size: 534,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/d48141187786931ec2cf8645e384be7878c7dc53",
    },
    {
      path: "resources/lang/en/passwords.php",
      mode: "100644",
      type: "blob",
      sha: "2345a56b5a6927a286e99ff80efc963ea3422e0c",
      size: 744,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/2345a56b5a6927a286e99ff80efc963ea3422e0c",
    },
    {
      path: "resources/lang/en/validation.php",
      mode: "100644",
      type: "blob",
      sha: "a65914f9d0f2ebc446876824a356c28d71b373f9",
      size: 7642,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/a65914f9d0f2ebc446876824a356c28d71b373f9",
    },
    {
      path: "resources/sass",
      mode: "040000",
      type: "tree",
      sha: "d408ea2824e0e4557de20000beeeb71657600f4e",
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/trees/d408ea2824e0e4557de20000beeeb71657600f4e",
    },
    {
      path: "resources/sass/app.scss",
      mode: "100644",
      type: "blob",
      sha: "0a514e4930db7fa062abc73d8c7f5bc921d8d447",
      size: 167,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/0a514e4930db7fa062abc73d8c7f5bc921d8d447",
    },
    {
      path: "resources/views",
      mode: "040000",
      type: "tree",
      sha: "1ed8d7e2e67a31d0af4ffbbad1892e13c38fecc2",
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/trees/1ed8d7e2e67a31d0af4ffbbad1892e13c38fecc2",
    },
    {
      path: "resources/views/auth",
      mode: "040000",
      type: "tree",
      sha: "e6d6cac45bdfbd7fe8770e1607a0dabbfb6786da",
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/trees/e6d6cac45bdfbd7fe8770e1607a0dabbfb6786da",
    },
    {
      path: "resources/views/auth/login.blade.php",
      mode: "100644",
      type: "blob",
      sha: "c12b97e57731b97b20bdacdc4710923f1ff5877f",
      size: 3378,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/c12b97e57731b97b20bdacdc4710923f1ff5877f",
    },
    {
      path: "resources/views/auth/passwords",
      mode: "040000",
      type: "tree",
      sha: "40ee2f937c3600b0470bca34fc18fcc53e41ffcd",
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/trees/40ee2f937c3600b0470bca34fc18fcc53e41ffcd",
    },
    {
      path: "resources/views/auth/passwords/confirm.blade.php",
      mode: "100644",
      type: "blob",
      sha: "ca78fc1d3c6fdbcdfc9554811eea58c124d2113e",
      size: 2075,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/ca78fc1d3c6fdbcdfc9554811eea58c124d2113e",
    },
    {
      path: "resources/views/auth/passwords/email.blade.php",
      mode: "100644",
      type: "blob",
      sha: "1fea98456d8393846b031bf4508913a862431bd9",
      size: 1914,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/1fea98456d8393846b031bf4508913a862431bd9",
    },
    {
      path: "resources/views/auth/passwords/reset.blade.php",
      mode: "100644",
      type: "blob",
      sha: "989931d3a20f846813f8bfbad204bc3bd1df8207",
      size: 3020,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/989931d3a20f846813f8bfbad204bc3bd1df8207",
    },
    {
      path: "resources/views/auth/register.blade.php",
      mode: "100644",
      type: "blob",
      sha: "d236a48ecb6d7ffb0d4a4fb62d7be35a79019cac",
      size: 3672,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/d236a48ecb6d7ffb0d4a4fb62d7be35a79019cac",
    },
    {
      path: "resources/views/auth/verify.blade.php",
      mode: "100644",
      type: "blob",
      sha: "9f8c1bc0f6f3964f69612694b8fe59695cad345b",
      size: 1134,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/9f8c1bc0f6f3964f69612694b8fe59695cad345b",
    },
    {
      path: "resources/views/home.blade.php",
      mode: "100644",
      type: "blob",
      sha: "1f34466b95e3e03b1e9a3cfe8bdb328c01f75363",
      size: 650,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/1f34466b95e3e03b1e9a3cfe8bdb328c01f75363",
    },
    {
      path: "resources/views/layouts",
      mode: "040000",
      type: "tree",
      sha: "51133aad6452605dcc0e98dcd8ec7cc6fe3d5e6a",
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/trees/51133aad6452605dcc0e98dcd8ec7cc6fe3d5e6a",
    },
    {
      path: "resources/views/layouts/app.blade.php",
      mode: "100644",
      type: "blob",
      sha: "79a86b07c64e650907bf8df550e567c0afd7a5f8",
      size: 3334,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/79a86b07c64e650907bf8df550e567c0afd7a5f8",
    },
    {
      path: "resources/views/welcome.blade.php",
      mode: "100644",
      type: "blob",
      sha: "cb24cbcbf01829fa863404345eb8a637e8ddbf6e",
      size: 968,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/cb24cbcbf01829fa863404345eb8a637e8ddbf6e",
    },
    {
      path: "routes",
      mode: "040000",
      type: "tree",
      sha: "55af80e3002860362b98fe7711089b522abb945c",
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/trees/55af80e3002860362b98fe7711089b522abb945c",
    },
    {
      path: "routes/api.php",
      mode: "100644",
      type: "blob",
      sha: "bcb8b18986204615608d8013c463c73f1fa5a76e",
      size: 566,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/bcb8b18986204615608d8013c463c73f1fa5a76e",
    },
    {
      path: "routes/channels.php",
      mode: "100644",
      type: "blob",
      sha: "963b0d2154cce55b79d5918f23666f1ec19c8df5",
      size: 551,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/963b0d2154cce55b79d5918f23666f1ec19c8df5",
    },
    {
      path: "routes/console.php",
      mode: "100644",
      type: "blob",
      sha: "da55196d406adee8ab6bb158f72670bc484b434d",
      size: 593,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/da55196d406adee8ab6bb158f72670bc484b434d",
    },
    {
      path: "routes/web.php",
      mode: "100644",
      type: "blob",
      sha: "0c16db425d7640e61850c7d9fea22875573eae9a",
      size: 569,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/0c16db425d7640e61850c7d9fea22875573eae9a",
    },
    {
      path: "server.php",
      mode: "100644",
      type: "blob",
      sha: "5fb6379e71f275a1784e6638adc96420aaa5c5b2",
      size: 563,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/5fb6379e71f275a1784e6638adc96420aaa5c5b2",
    },
    {
      path: "storage",
      mode: "040000",
      type: "tree",
      sha: "fc752eb01f1bb3c4c469a72c8f5a4de1ed2ad2ed",
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/trees/fc752eb01f1bb3c4c469a72c8f5a4de1ed2ad2ed",
    },
    {
      path: "storage/app",
      mode: "040000",
      type: "tree",
      sha: "3cf5e09286183fa233fe39d26dad9f902fc1c69e",
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/trees/3cf5e09286183fa233fe39d26dad9f902fc1c69e",
    },
    {
      path: "storage/app/.gitignore",
      mode: "100644",
      type: "blob",
      sha: "8f4803c05638697d84ea28d40693324ec70f7990",
      size: 23,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/8f4803c05638697d84ea28d40693324ec70f7990",
    },
    {
      path: "storage/app/public",
      mode: "040000",
      type: "tree",
      sha: "96233b34ccba706a9f89dca87a9282a3cd836e0a",
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/trees/96233b34ccba706a9f89dca87a9282a3cd836e0a",
    },
    {
      path: "storage/app/public/.gitignore",
      mode: "100644",
      type: "blob",
      sha: "d6b7ef32c8478a48c3994dcadc86837f4371184d",
      size: 14,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/d6b7ef32c8478a48c3994dcadc86837f4371184d",
    },
    {
      path: "storage/framework",
      mode: "040000",
      type: "tree",
      sha: "a0c778ac28336191db975488503b3f013b1c7926",
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/trees/a0c778ac28336191db975488503b3f013b1c7926",
    },
    {
      path: "storage/framework/.gitignore",
      mode: "100644",
      type: "blob",
      sha: "b02b700f1bfc86318cff8f4c4a7f0820974d464a",
      size: 103,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/b02b700f1bfc86318cff8f4c4a7f0820974d464a",
    },
    {
      path: "storage/framework/cache",
      mode: "040000",
      type: "tree",
      sha: "32e46a3cd15b9aa54cccc46fc53990f382062325",
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/trees/32e46a3cd15b9aa54cccc46fc53990f382062325",
    },
    {
      path: "storage/framework/cache/.gitignore",
      mode: "100644",
      type: "blob",
      sha: "01e4a6cda9eb380973b23a40d562bca8a3a198b4",
      size: 21,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/01e4a6cda9eb380973b23a40d562bca8a3a198b4",
    },
    {
      path: "storage/framework/cache/data",
      mode: "040000",
      type: "tree",
      sha: "96233b34ccba706a9f89dca87a9282a3cd836e0a",
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/trees/96233b34ccba706a9f89dca87a9282a3cd836e0a",
    },
    {
      path: "storage/framework/cache/data/.gitignore",
      mode: "100644",
      type: "blob",
      sha: "d6b7ef32c8478a48c3994dcadc86837f4371184d",
      size: 14,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/d6b7ef32c8478a48c3994dcadc86837f4371184d",
    },
    {
      path: "storage/framework/sessions",
      mode: "040000",
      type: "tree",
      sha: "96233b34ccba706a9f89dca87a9282a3cd836e0a",
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/trees/96233b34ccba706a9f89dca87a9282a3cd836e0a",
    },
    {
      path: "storage/framework/sessions/.gitignore",
      mode: "100644",
      type: "blob",
      sha: "d6b7ef32c8478a48c3994dcadc86837f4371184d",
      size: 14,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/d6b7ef32c8478a48c3994dcadc86837f4371184d",
    },
    {
      path: "storage/framework/testing",
      mode: "040000",
      type: "tree",
      sha: "96233b34ccba706a9f89dca87a9282a3cd836e0a",
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/trees/96233b34ccba706a9f89dca87a9282a3cd836e0a",
    },
    {
      path: "storage/framework/testing/.gitignore",
      mode: "100644",
      type: "blob",
      sha: "d6b7ef32c8478a48c3994dcadc86837f4371184d",
      size: 14,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/d6b7ef32c8478a48c3994dcadc86837f4371184d",
    },
    {
      path: "storage/framework/views",
      mode: "040000",
      type: "tree",
      sha: "96233b34ccba706a9f89dca87a9282a3cd836e0a",
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/trees/96233b34ccba706a9f89dca87a9282a3cd836e0a",
    },
    {
      path: "storage/framework/views/.gitignore",
      mode: "100644",
      type: "blob",
      sha: "d6b7ef32c8478a48c3994dcadc86837f4371184d",
      size: 14,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/d6b7ef32c8478a48c3994dcadc86837f4371184d",
    },
    {
      path: "storage/logs",
      mode: "040000",
      type: "tree",
      sha: "96233b34ccba706a9f89dca87a9282a3cd836e0a",
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/trees/96233b34ccba706a9f89dca87a9282a3cd836e0a",
    },
    {
      path: "storage/logs/.gitignore",
      mode: "100644",
      type: "blob",
      sha: "d6b7ef32c8478a48c3994dcadc86837f4371184d",
      size: 14,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/d6b7ef32c8478a48c3994dcadc86837f4371184d",
    },
    {
      path: "tailwind.config.js",
      mode: "100644",
      type: "blob",
      sha: "c9e636b5a6d96ee3f980e94680540c9c51a3787e",
      size: 256,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/c9e636b5a6d96ee3f980e94680540c9c51a3787e",
    },
    {
      path: "tests",
      mode: "040000",
      type: "tree",
      sha: "d50aa6e9b745b0dba78abd8fa5b1c9a57946a46a",
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/trees/d50aa6e9b745b0dba78abd8fa5b1c9a57946a46a",
    },
    {
      path: "tests/CreatesApplication.php",
      mode: "100644",
      type: "blob",
      sha: "547152f6a933b1c1f409283d7bdfe1ba556d4069",
      size: 380,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/547152f6a933b1c1f409283d7bdfe1ba556d4069",
    },
    {
      path: "tests/Feature",
      mode: "040000",
      type: "tree",
      sha: "ddd04bc5997775f80081ec032074ddd9c98c70e3",
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/trees/ddd04bc5997775f80081ec032074ddd9c98c70e3",
    },
    {
      path: "tests/Feature/ExampleTest.php",
      mode: "100644",
      type: "blob",
      sha: "cdb5111934c530977ccae72ac0c405c4582cfb2f",
      size: 340,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/cdb5111934c530977ccae72ac0c405c4582cfb2f",
    },
    {
      path: "tests/TestCase.php",
      mode: "100644",
      type: "blob",
      sha: "2932d4a69d6554cec4dea94e3194351710bd659e",
      size: 163,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/2932d4a69d6554cec4dea94e3194351710bd659e",
    },
    {
      path: "tests/Unit",
      mode: "040000",
      type: "tree",
      sha: "a25b5b29351b05f4ae2fc3c533fb1e89b54ab530",
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/trees/a25b5b29351b05f4ae2fc3c533fb1e89b54ab530",
    },
    {
      path: "tests/Unit/ExampleTest.php",
      mode: "100644",
      type: "blob",
      sha: "358cfc8834abf47c2467b90ad79771edd7eda517",
      size: 255,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/358cfc8834abf47c2467b90ad79771edd7eda517",
    },
    {
      path: "webpack.mix.js",
      mode: "100644",
      type: "blob",
      sha: "f1fa047d2b819e9695d0d85af5d85b19e5c5f231",
      size: 696,
      url:
        "https://api.github.com/repos/fuadnafiz98/laravel-react-starter/git/blobs/f1fa047d2b819e9695d0d85af5d85b19e5c5f231",
    },
  ],
  truncated: false,
};
