module.exports = {
  future: {
    // removeDeprecatedGapUtilities: true,
    // purgeLayersByDefault: true,
  },
  purge: [],
  theme: {
    fontFamily: {
      sans: ["Inter"],
    },
    extend: {
      colors: {
        "nw-bg": "#011627",
        "nw-fg": "#d6deeb",
        "nw-black": "#575656",
        "nw-red": "#ef5350",
        "nw-orange": "#f78c6c",
        "nw-yellow": "#addb67 ",
        "nw-cyan": "#21c7a8",
        "nw-blue": "#82aaff",
        "nw-green": "#22da6e",
      },
    },
  },
  variants: {},
  plugins: [],
};
